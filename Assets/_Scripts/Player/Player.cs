﻿using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Player character
    /// </summary>
    [RequireComponent(typeof(Vitals))]
    [RequireComponent(typeof(PlayerController))]
    public class Player : MonoBehaviour
    {

        // TODO: Race, Class, SecondaryClass, GenderEnum, Alignment, Abilities

        /// <summary>
        /// Character vitals (health, mana, etc)
        /// </summary>
        [HideInInspector]
        public Vitals Vitals;

        /// <summary>
        /// Player motor controller (movement)
        /// </summary>
        [HideInInspector]
        public PlayerController Controller;


        private void Start()
        {
            Vitals = GetComponent<Vitals>();
            Controller = GetComponent<PlayerController>();
        }
    }
}
