﻿using UnityEngine;
using UnityEngine.UI;


namespace RPGBase
{
    /// <summary>
    /// Player head's-up-display UI
    /// </summary>
    public class PlayerHud : MonoBehaviour
    {
        [SerializeField] private Player player;
        [SerializeField] private DebugHud debugHud;
        [SerializeField] private Slider healthSlider;
        [SerializeField] private Slider hungerSlider;
        [SerializeField] private Slider staminaSlider;
        [SerializeField] private Slider thirstSlider;
        [SerializeField] private Text compassText;

        // TODO: Add ability to update maximum values (pub/sub)


        // Use this for initialization
        void Start()
        {
            healthSlider.value = player.Vitals.Health.Value;
            healthSlider.maxValue = player.Vitals.Health.MaxValue;

            hungerSlider.value = player.Vitals.Hunger.Value;
            hungerSlider.maxValue = player.Vitals.Hunger.MaxValue;

            staminaSlider.value = player.Vitals.Stamina.Value;
            staminaSlider.maxValue = player.Vitals.Stamina.MaxValue;

            thirstSlider.value = player.Vitals.Thirst.Value;
            thirstSlider.maxValue = player.Vitals.Thirst.MaxValue;
        }


        // Update is called once per frame
        void Update()
        {
            healthSlider.value = player.Vitals.Health.Value;
            hungerSlider.value = player.Vitals.Hunger.Value;
            staminaSlider.value = player.Vitals.Stamina.Value;
            thirstSlider.value = player.Vitals.Thirst.Value;
            compassText.text = player.Controller.CompassDirection;
        }
    }
}
