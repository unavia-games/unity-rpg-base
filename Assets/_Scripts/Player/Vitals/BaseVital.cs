﻿namespace RPGBase
{
    /// <summary>
    /// Base player statistic (vital)
    /// </summary>
    public class BaseVital
    {
        /// <summary>
        /// Parent player vitals
        /// </summary>
        /// <value>Parent player vitals</value>
        protected Vitals parentPlayerVitals { get; private set; }

        /// <summary>
        /// Set the relation to the parent Vitals object (used to manipulate other vital values)
        /// </summary>
        /// <param name="parentVitals">Parent object</param>
        public void Init(Vitals parentVitals)
        {
            parentPlayerVitals = parentVitals;
        }
    }
}