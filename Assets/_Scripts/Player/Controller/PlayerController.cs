﻿using UnityEngine;

namespace RPGBase
{
    /// <summary>
    /// Control for player movement in response to input
    /// </summary>
    [RequireComponent(typeof(Player))]
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(CapsuleCollider))]
    public class PlayerController : MonoBehaviour
    {
        /// <summary>
        /// Player camera reference
        /// </summary>
        public Camera Camera;
        /// <summary>
        /// Advanced player controller settings reference
        /// </summary>
        public AdvancedSettings AdvancedSettings = new AdvancedSettings();
        /// <summary>
        /// Player controller movement settings reference
        /// </summary>
        public MovementSettings MovementSettings = new MovementSettings();
        /// <summary>
        /// Player controller mouse manager reference
        /// </summary>
        public MouseLook MouseLook = new MouseLook();

        /// <summary>
        ///  Whether the player can run
        /// </summary>
        [HideInInspector]
        public bool CanRun
        {
            get
            {
                return player.Vitals.Stamina.Value > 0;
            }
        }
        /// <summary>
        /// Whether the player is running
        /// </summary>
        [HideInInspector]
        public bool Running = false;
        /// <summary>
        /// Whether the player is walking
        /// </summary>
        [HideInInspector]
        public bool Walking = false;
        /// <summary>
        /// Player compass heading
        /// </summary>
        public string CompassDirection { get; private set; }
        /// <summary>
        /// Player velocity
        /// </summary>
        public Vector3 Velocity
        {
            get { return rigidBody.velocity; }
        }
        /// <summary>
        /// Whether the player is grounded
        /// </summary>
        public bool Grounded { get; private set; }
        /// <summary>
        /// Whether the player is falling
        /// </summary>
        public bool Falling
        {
            get { return !Grounded; }
        }
        /// <summary>
        /// Whether the player is jumping
        /// </summary>
        public bool Jumping { get; private set; }
        /// <summary>
        /// Whether the player is moving (running, walking, jumping, etc)
        /// </summary>
        public bool Moving
        {
            get { return Running || Walking || Jumping; }
        }

        private Rigidbody rigidBody;
        private CapsuleCollider capsule;
        private Player player;
        private float yRotation;
        private Vector3 groundContactNormal;
        private bool shouldJump;
        private bool previouslyGrounded;


        private void Start()
        {
            player = GetComponent<Player>();
            rigidBody = GetComponent<Rigidbody>();
            capsule = GetComponent<CapsuleCollider>();
            MouseLook.Init(transform, Camera.transform);
            MovementSettings.Init(this);
        }


        private void Update()
        {
            // Camera/player rotation is calculated from mouse position
            RotateView();

            // Handle jumping (cannot jump again while currently jumping)
            if (Input.GetKey(MovementSettings.JumpKey) && !shouldJump)
            {
                shouldJump = true;
            }
        }


        private void FixedUpdate()
        {
            GroundCheck();
            Vector2 input = GetInput();

            // Move camera along the direction it is facing
            if ((Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon) && (AdvancedSettings.AirControl || Grounded))
            {
                // Camera is always facing forward (can never go another direction)
                Vector3 desiredMove = Camera.transform.forward * input.y + Camera.transform.right * input.x;
                desiredMove = Vector3.ProjectOnPlane(desiredMove, groundContactNormal).normalized;

                desiredMove.x = desiredMove.x * MovementSettings.CurrentTargetSpeed;
                desiredMove.z = desiredMove.z * MovementSettings.CurrentTargetSpeed;
                desiredMove.y = desiredMove.y * MovementSettings.CurrentTargetSpeed;

                if (rigidBody.velocity.sqrMagnitude <
                    (MovementSettings.CurrentTargetSpeed * MovementSettings.CurrentTargetSpeed))
                {
                    rigidBody.AddForce(desiredMove * SlopeMultiplier(), ForceMode.Impulse);
                }
            }

            // Control player jumping (only when previously grounded)
            if (Grounded)
            {
                rigidBody.drag = 5f;

                if (shouldJump)
                {
                    rigidBody.drag = 0f;
                    rigidBody.velocity = new Vector3(rigidBody.velocity.x, 0f, rigidBody.velocity.z);
                    rigidBody.AddForce(new Vector3(0f, MovementSettings.JumpForce, 0f), ForceMode.Impulse);
                    Jumping = true;
                }

                if (!Jumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && rigidBody.velocity.magnitude < 1f)
                {
                    rigidBody.Sleep();
                }
            }
            // Keep player on ground if not jumping (and previously on the ground)
            else
            {
                rigidBody.drag = 0f;
                if (previouslyGrounded && !Jumping)
                {
                    StickToGroundHelper();
                }
            }

            shouldJump = false;
        }


        /// <summary>
        /// Determine how much to slow the player based on steeper slopes
        /// </summary>
        /// <returns>Slope slowness multiplier</returns>
        private float SlopeMultiplier()
        {
            float angle = Vector3.Angle(groundContactNormal, Vector3.up);
            return MovementSettings.SlopeCurveModifier.Evaluate(angle);
        }


        /// <summary>
        /// Keep the player attached to ground (not sinking through)
        /// </summary>
        private void StickToGroundHelper()
        {
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - AdvancedSettings.ShellOffset), Vector3.down, out hitInfo,
                                   ((capsule.height / 2f) - capsule.radius) +
                                   AdvancedSettings.StickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
                {
                    rigidBody.velocity = Vector3.ProjectOnPlane(rigidBody.velocity, hitInfo.normal);
                }
            }
        }


        /// <summary>
        /// Get direction input from keyboard
        /// </summary>
        /// <returns>Vector representing directional input</returns>
        private Vector2 GetInput()
        {
            Vector2 input = new Vector2
            {
                x = Input.GetAxis("Horizontal"),
                y = Input.GetAxis("Vertical")
            };

            MovementSettings.UpdateDesiredTargetSpeed(input);
            return input;
        }


        /// <summary>
        /// Rotate the camera to match the mouse direction
        /// </summary>
        private void RotateView()
        {
            // Avoids the mouse looking if the game is effectively paused
            if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

            // Get the rotation before it's changed
            float oldYRotation = transform.eulerAngles.y;

            MouseLook.LookRotation(transform, Camera.transform);

            if (Grounded || AdvancedSettings.AirControl)
            {
                // Rotate the rigidbody velocity to match the new direction that the character is looking
                Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
                rigidBody.velocity = velRotation * rigidBody.velocity;
            }

            UpdateCompassHeading();
        }

        /// <summary>
        /// Check whether the collider capsule is colliding at the bottom (user is grounded)
        /// </summary>
        private void GroundCheck()
        {
            previouslyGrounded = Grounded;
            RaycastHit hitInfo;

            if (Physics.SphereCast(transform.position, capsule.radius * (1.0f - AdvancedSettings.ShellOffset), Vector3.down, out hitInfo,
                                   ((capsule.height / 2f) - capsule.radius) + AdvancedSettings.GroundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                Grounded = true;
                groundContactNormal = hitInfo.normal;
            }
            else
            {
                Grounded = false;
                groundContactNormal = Vector3.up;
            }

            if (!previouslyGrounded && Grounded && Jumping)
            {
                Jumping = false;
            }
        }


        /// <summary>
        /// Update the player compass heading
        /// </summary>
        private void UpdateCompassHeading()
        {
            const float HALF_QUADRANT = 22.5f;

            // Zero-out the y-axis component of the forward vector to get direction in "x,y" planes
            Vector3 forward = Camera.transform.forward;
            forward.y = 0;

            // TODO: Send HUD-change notification only when compass heading changes

            float compassAngle = Quaternion.LookRotation(forward).eulerAngles.y;

            if (compassAngle < HALF_QUADRANT || compassAngle > 360 - HALF_QUADRANT)
            {
                CompassDirection = "W";
            }
            else if (compassAngle < 45 + HALF_QUADRANT)
            {
                CompassDirection = "NW";
            }
            else if (compassAngle < 90 + HALF_QUADRANT)
            {
                CompassDirection = "N";
            }
            else if (compassAngle < 135 + HALF_QUADRANT)
            {
                CompassDirection = "NE";
            }
            else if (compassAngle < 180 + HALF_QUADRANT)
            {
                CompassDirection = "E";
            }
            else if (compassAngle < 225 + HALF_QUADRANT)
            {
                CompassDirection = "SE";
            }
            else if (compassAngle < 270 + HALF_QUADRANT)
            {
                CompassDirection = "S";
            }
            else if (compassAngle < 315 + HALF_QUADRANT)
            {
                CompassDirection = "SW";
            }
            else
            {
                CompassDirection = "XX";
            }
        }
    }
}
