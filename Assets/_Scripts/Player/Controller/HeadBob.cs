﻿using UnityEngine;
using UnityStandardAssets.Utility;

// TODO: Document class

namespace RPGBase
{
    /// <summary>
    /// First person camera head bob effect
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class HeadBob : MonoBehaviour
    {
        public CurveControlledBob motionBob = new CurveControlledBob();
        public LerpControlledBob jumpAndLandingBob = new LerpControlledBob();
        public PlayerController playerController;
        public float StrideInterval;
        [Range(0f, 1f)] public float RunningStrideLengthen;

        // private CameraRefocus m_CameraRefocus;
        private Camera camera;
        private bool m_PreviouslyGrounded;
        private Vector3 m_OriginalCameraPosition;


        private void Start()
        {
            camera = GetComponent<Camera>();
            motionBob.Setup(camera, StrideInterval);
            m_OriginalCameraPosition = camera.transform.localPosition;
            //     m_CameraRefocus = new CameraRefocus(Camera, transform.root.transform, Camera.transform.localPosition);
        }


        private void Update()
        {
            //  m_CameraRefocus.GetFocusPoint();
            Vector3 newCameraPosition;
            if (playerController.Velocity.magnitude > 0 && playerController.Grounded)
            {
                camera.transform.localPosition = motionBob.DoHeadBob(playerController.Velocity.magnitude * (playerController.Running ? RunningStrideLengthen : 1f));
                newCameraPosition = camera.transform.localPosition;
                newCameraPosition.y = camera.transform.localPosition.y - jumpAndLandingBob.Offset();
            }
            else
            {
                newCameraPosition = camera.transform.localPosition;
                newCameraPosition.y = m_OriginalCameraPosition.y - jumpAndLandingBob.Offset();
            }
            camera.transform.localPosition = newCameraPosition;

            if (!m_PreviouslyGrounded && playerController.Grounded)
            {
                StartCoroutine(jumpAndLandingBob.DoBobCycle());
            }

            m_PreviouslyGrounded = playerController.Grounded;
            //  m_CameraRefocus.SetFocusPoint();
        }
    }
}
