﻿using UnityEngine;
using UnityEngine.UI;

namespace RPGBase
{
    /// <summary>
    /// Debug information display
    /// </summary>
    public class DebugHud : MonoBehaviour
    {
        [SerializeField] private GameObject DebugPanel;
        [SerializeField] private Player Player;
        [SerializeField] private Toggle FallingToggle;
        [SerializeField] private Toggle GroundedToggle;
        [SerializeField] private Toggle JumpingToggle;
        [SerializeField] private Toggle MovingToggle;
        [SerializeField] private Toggle RunningToggle;
        [SerializeField] private Toggle WalkingToggle;
        [SerializeField] private Toggle MortalToggle;

        // Use this for initialization
        void Start()
        {
            FallingToggle.interactable = false;
            GroundedToggle.interactable = false;
            JumpingToggle.interactable = false;
            MovingToggle.interactable = false;
            RunningToggle.interactable = false;
            WalkingToggle.interactable = false;
        }

        // Update is called once per frame
        void Update()
        {
            FallingToggle.isOn = Player.Controller.Falling;
            GroundedToggle.isOn = Player.Controller.Grounded;
            JumpingToggle.isOn = Player.Controller.Jumping;
            MovingToggle.isOn = Player.Controller.Moving;
            RunningToggle.isOn = Player.Controller.Running;
            WalkingToggle.isOn = Player.Controller.Walking;
            MortalToggle.isOn = Player.Vitals.Mortal;

            // Manage visibility of Debug HUD
            if (Input.GetKeyDown(KeyCode.Backslash))
            {
                ToggleDebugHud();
            }
        }

        // Toggle Debug HUD visibility
        void ToggleDebugHud()
        {
            DebugPanel.SetActive(!DebugPanel.activeSelf);
        }
    }
}
